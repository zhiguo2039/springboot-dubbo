package com.yzg.study.dubbo.provider.service.impl;

import com.yzg.study.dubbo.api.service.BalanceService;
import com.yzg.study.dubbo.api.service.vo.Balance;
import org.apache.dubbo.config.annotation.DubboService;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 *dubbo的負載均衡策略：随机算法（random）、轮询算法(roundrobin)、最小活跃数算法(leastActive)、一致hash算法(consistentHash)
 */
@DubboService
public class BalanceServiceImpl implements BalanceService {


    final static Map<Integer, Balance> balanceMap = new HashMap() {{
        put(1, new Balance(1, 10, 1000));
        put(2, new Balance(2, 0, 10000));
        put(3, new Balance(3, 100, 0));
    }
    };

    @Override
    public Balance getBalance(Integer id) {
        System.out.println("调用20880的dubbo服务");
        if(id != null && balanceMap.containsKey(id)) {
            return balanceMap.get(id);
        }
        return new Balance(0, 0, 0, "不存在");
    }

}
