package com.yzg.study.dubbo.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.concurrent.CountDownLatch;


@SpringBootApplication
@EnableDiscoveryClient
@RefreshScope
public class DubboProviderApplication {

    //使用jar方式打包的启动方式
    private static CountDownLatch countDownLatch = new CountDownLatch(1);
    public static void main(String[] args) throws InterruptedException{
        SpringApplication.run(DubboProviderApplication.class, args).registerShutdownHook();
        countDownLatch.await();
    }

}
