package com.yzg.study.dubbo.provider.service.impl;

import com.alibaba.nacos.client.naming.utils.RandomUtils;
import com.yzg.study.dubbo.api.service.SayHelloService;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.config.annotation.Method;

import java.util.concurrent.TimeUnit;


@DubboService
public class SayHelloServiceImpl implements SayHelloService {

    public String sayHello(String name) {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return name + " provider" + RandomUtils.nextInt();
    }

}
