package com.yzg.study.dubbo.api.service.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Balance implements Serializable {
    private int id;
    private int diamond;
    private int ticket;
    private String message;

    public Balance(int id, int diamond, int ticket, String message) {
        this.id = id;
        this.diamond = diamond;
        this.ticket = ticket;
        this.message = message;
    }

    public Balance(int id, int diamond, int ticket) {
        this.id = id;
        this.diamond = diamond;
        this.ticket = ticket;
    }

    @Override
    public String toString() {
        return "Balance{" +
                "id=" + id +
                ", diamond=" + diamond +
                ", ticket=" + ticket +
                ", message='" + message + '\'' +
                '}';
    }
}
