package com.yzg.study.dubbo.api.service;

public interface SayHelloService {
    String sayHello(String name);
}
