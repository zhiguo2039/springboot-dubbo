package com.yzg.study.dubbo.api.service;

import com.yzg.study.dubbo.api.service.vo.Balance;

public interface BalanceService {
    Balance getBalance(Integer id);
}
