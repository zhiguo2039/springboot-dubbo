package com.yzg.study.dubbo.consumer.controller;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.yzg.study.dubbo.api.service.BalanceService;
import com.yzg.study.dubbo.api.service.SayHelloService;
import com.yzg.study.dubbo.api.service.vo.Balance;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.Method;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;


@RestController
@RequestMapping("/consumer")
public class ConsumeController {

    @DubboReference(methods = {@Method(name = "sayHello", timeout = 1000)})
    SayHelloService sayHelloService;

    @DubboReference(cluster = "failover", retries = 2, loadbalance = "random",timeout = 2000,mock = "com.yzg.study.dubbo.consumer.mock.MockBalanceService")
    BalanceService balanceService;

    @RequestMapping("/account")
    public Balance getAccount(@RequestParam Integer id) {
        return balanceService.getBalance(id);
    }

    @GetMapping("/sayHello")
    @HystrixCommand(fallbackMethod = "sayHelloHystrix")
    public String sayHello(){
        return sayHelloService.sayHello("Hello Springcloud-Dubbo!");
    }

    private String sayHelloHystrix(){
        return "wait moment,your service is approaching!";
    }

    @GetMapping("/catchException")
    public Integer catchException(){
        int res = 1/0;
        return res;
    }
}
