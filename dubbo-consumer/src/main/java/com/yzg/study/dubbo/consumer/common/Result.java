package com.yzg.study.dubbo.consumer.common;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.google.common.base.Objects;
import com.yzg.study.dubbo.consumer.exception.ArgsException;
import com.yzg.study.dubbo.consumer.exception.ServiceException;
import com.yzg.study.dubbo.consumer.exception.MsgException;
import com.yzg.study.dubbo.consumer.utils.ErrorUtil;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

public class Result<T> implements Serializable {

    private static final long serialVersionUID = -9141307870732228707L;
    public static final String SUCCESS = "success";
    public static final String ERROR = "error";
    private String state;
    private String msg;
    private T obj;

    public Result(T obj) {
        this.state = "success";
        this.obj = obj;
    }

    public Result(ServiceException e) {
        this.state = e.getMessage();
        this.msg = e.getErrorInfo();
    }

    public Result(ArgsException e) {
        this.state = e.getMessage();
        this.msg = e.getErrorInfo();
    }

    public Result(MsgException e) {
        this.state = "error";
        this.msg = e.getErrorInfo();
    }

    public Result(Throwable e) {
        this.state = "error";
        this.msg = ErrorUtil.getErrorInfo("EF00000");
    }

    public Result<T> success(T obj) {
        this.state = "success";
        this.obj = obj;
        return this;
    }

    public Result<T> error(String msg) {
        this.state = "error";
        this.msg = msg;
        return this;
    }

    public Result<T> error() {
        this.state = "error";
        this.msg = ErrorUtil.getErrorInfo("EF00000");
        return this;
    }

    /**
     * 静态方法
     * @return
     */
    public static Result fail(String msg){
        return new Result<>("error", msg, null);
    }
    public static Result fail(){
        return new Result("error", "操作失败", null);
    }
    public static Result ok(){
        return new Result("success", "操作成功", null);
    }
    public static <T> Result<T> ok(T obj){
        return new Result<T>("success", "操作成功", obj);
    }

    /**
     * 登录失效
     * @return
     */
    public static Result logonFailure(String msg){
        return new Result(
                "2",
                StringUtils.isNotEmpty(msg)? msg :"用户失效，请重新登录",
                null);
    }

    @JSONField(serialize = false)
    public boolean isSuccess() {
        return Objects.equal("success", this.state);
    }

    public String toJson() {
        return JSON.toJSONString(this);
    }

    public static <T> Result.RBuilder<T> builder() {
        return new Result.RBuilder();
    }

    public Result(final String state, final String msg, final T obj) {
        this.state = state;
        this.msg = msg;
        this.obj = obj;
    }

    public Result() {
    }

    public String getState() {
        return this.state;
    }

    public String getMsg() {
        return this.msg;
    }

    public T getObj() {
        return this.obj;
    }

    public Result<T> setState(final String state) {
        this.state = state;
        return this;
    }

    public Result<T> setMsg(final String msg) {
        this.msg = msg;
        return this;
    }

    public Result<T> setObj(final T obj) {
        this.obj = obj;
        return this;
    }


    public static class RBuilder<T> {
        private String state;
        private String msg;
        private T obj;

        RBuilder() {
        }

        public Result.RBuilder<T> state(final String state) {
            this.state = state;
            return this;
        }

        public Result.RBuilder<T> msg(final String msg) {
            this.msg = msg;
            return this;
        }

        public Result.RBuilder<T> obj(final T obj) {
            this.obj = obj;
            return this;
        }

        public Result<T> build() {
            return new Result(this.state, this.msg, this.obj);
        }

        public String toString() {
            return "Result.RBuilder(state=" + this.state + ", msg=" + this.msg + ", obj=" + this.obj + ")";
        }
    }

}
