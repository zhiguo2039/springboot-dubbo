package com.yzg.study.dubbo.consumer.exception;


import com.yzg.study.dubbo.consumer.utils.ErrorUtil;

public class ArgsException extends ServiceException {

    private static final long serialVersionUID = 1L;
    private final Object[] args;

    public ArgsException(String msg, Object... args) {
        super(msg);
        this.args = args;
    }

    public ArgsException(String msg, Throwable cause, Object... args) {
        super(msg, cause);
        this.args = args;
    }

    public String getErrorInfo() {
        String errorInfo = ErrorUtil.getErrorInfo(this.getMessage());
        int i = 0;

        for(int len = this.args.length; i < len; ++i) {
            String arg = String.valueOf(this.args[i]);
            errorInfo = errorInfo.replace("{" + i + "}", arg);
        }

        return errorInfo;
    }

}
