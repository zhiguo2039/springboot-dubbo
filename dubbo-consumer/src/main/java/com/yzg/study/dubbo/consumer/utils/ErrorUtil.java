package com.yzg.study.dubbo.consumer.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;

public class ErrorUtil {

    @Autowired
    private Environment env;
    private static Environment ERROR_INFO;

    public ErrorUtil() {
    }

    @PostConstruct
    public void init() {
        ERROR_INFO = this.env;
    }

    public static String getErrorInfo(String errorCode) {
        String value = ERROR_INFO.getProperty(errorCode);
        return StringUtils.isEmpty(value) ? "系统异常，请稍后再试！" : ERROR_INFO.getProperty(errorCode);
    }

    public static String getErrorInfo(String errorCode, Object o) {
        return getErrorInfo(errorCode).replace("{0}", String.valueOf(o));
    }

    public static String getErrorInfo(String errorCode, Object o1, Object o2) {
        return getErrorInfo(errorCode, o1).replace("{1}", String.valueOf(o2));
    }

    public static String getErrorInfo(String errorCode, Object o1, Object o2, Object... args) {
        String errorInfo = getErrorInfo(errorCode, o1, o2);
        int i = 0;

        for(int len = args.length; i < len; ++i) {
            String arg = String.valueOf(args[i]);
            errorInfo = errorInfo.replace("{" + (i + 2) + "}", arg);
        }

        return errorInfo;
    }

}
