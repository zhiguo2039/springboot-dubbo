package com.yzg.study.dubbo.consumer.exception;

import com.yzg.study.dubbo.consumer.common.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

@RestControllerAdvice
@Slf4j
public class GlobalException extends RuntimeException{

    public GlobalException() {
    }

    public GlobalException(String message) {
        super(message);
    }

    @ExceptionHandler({Exception.class})
    @ResponseBody
    public Result<Exception> handleException(Exception e){
        log.error("全局异常信息 ex={}", e.getMessage(),e);
        return Result.fail("系统异常，请稍后重试");
    }

    @ExceptionHandler({ServiceException.class})
    @ResponseBody
    public Result<Exception> handleException(ServiceException e) {
        log.error("全局异常信息 ex={}", e.getMessage(), e);
        return Result.fail("请求服务异常");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public Result<Exception> checkBodyException(MethodArgumentNotValidException exception) {
        BindingResult result = exception.getBindingResult();
        String message = "";
        if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
            if (errors != null) {
                errors.forEach(p -> {
                    FieldError fieldError = (FieldError) p;
                    log.error("Data check failure : object{" + fieldError.getObjectName() + "},field{" + fieldError.getField() +
                            "},errorMessage{" + fieldError.getDefaultMessage() + "}");

                });
                if (errors.size() > 0) {
                    FieldError fieldError = (FieldError) errors.get(0);
                    message = fieldError.getDefaultMessage();
                }
            }
        }
        return Result.fail("".equals(message) ? "参数错误" : message);
    }

    @ExceptionHandler(HttpMessageConversionException.class)
    public Result<Exception> parameterTypeException(HttpMessageConversionException exception) {
        log.error(exception.getCause().getLocalizedMessage());
        return Result.fail("类型转换错误");

    }
}
