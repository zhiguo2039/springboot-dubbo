package com.yzg.study.dubbo.consumer.mock;

import com.yzg.study.dubbo.api.service.BalanceService;
import com.yzg.study.dubbo.api.service.vo.Balance;


public class MockBalanceService implements BalanceService {
    @Override
    public Balance getBalance(Integer id) {
        Balance balance = new Balance(-1,-1,-1,"您的请求正在快马加鞭赶过来");
        return balance;
    }
}
