package com.yzg.study.dubbo.consumer.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
@Slf4j
public class RequestExecuteAop {

    @Pointcut("execution(* com.yzg.study.dubbo.consumer.controller.*.*(..))")
    private void pointCutMethodController(){

    }

    @Around("pointCutMethodController()")
    public Object doAroundService(ProceedingJoinPoint joinPoint) throws Throwable {
        long begin = System.nanoTime();
        //继续执行目标方法
        Object obj = joinPoint.proceed();
        long end = System.nanoTime();
        log.info("Controller method：{}，prams：{}，cost time：{} ns，cost：{} ms",
                joinPoint.getSignature().toString(), Arrays.toString(joinPoint.getArgs()),
                (end - begin), (end - begin) / 1000000);
        return obj;
    }

    @Before("pointCutMethodController()")
    public void doBeforeService(JoinPoint point){

        System.out.println("方法执行前============》before");
        log.info("拦截的方法名" + point.getSignature().getName());
        log.info("拦截的方法参数" + point.getArgs());

    }

    @After("pointCutMethodController()")
    public void doAfterService( ){
        System.out.println("方法执行后============》after");
    }

}
