package com.yzg.study.dubbo.consumer.exception;

public class MsgException extends ServiceException {

    private static final long serialVersionUID = -3056331142702492056L;

    public MsgException() {
    }

    public MsgException(String msg) {
        super(msg);
    }

    public MsgException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public MsgException(Throwable cause) {
        super(cause);
    }

    public String getErrorInfo() {
        return this.getMessage();
    }

}
