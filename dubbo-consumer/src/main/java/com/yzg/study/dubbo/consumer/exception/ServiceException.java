package com.yzg.study.dubbo.consumer.exception;

import com.yzg.study.dubbo.consumer.utils.ErrorUtil;

public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    public static final String ERROR_CODE = "EF00000";

    public ServiceException() {
        super("EF00000");
    }

    public ServiceException(String msg) {
        super(msg);
    }

    public ServiceException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public String getErrorInfo() {
        return ErrorUtil.getErrorInfo(this.getMessage());
    }

}